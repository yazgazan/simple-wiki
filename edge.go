
package main

type Edge struct{
  Type  int

  From  string  `json:"_from,omitempty"`
  To    string  `json:"_to,omitempty"`
}

