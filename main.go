
package main

import (
  "fmt"

  "github.com/gin-gonic/gin"
  "github.com/yazgazan/konek/arango-connector"
)

const (
  C_Listen = "localhost:8085"

  C_Arango_Addr = "http://localhost:8529"
  C_Arango_User = "root"
  C_Arango_Passwd = "kidoo blur carcrash airplane"
  C_Arango_Db = "simple-wiki"
)

func Arango() *arango.Database {
  ar := arango.New(C_Arango_Addr)
  ar.User = C_Arango_User
  ar.Passwd = C_Arango_Passwd

  return ar.Use(C_Arango_Db)
}

func main() {
  r := gin.Default()

  api := r.Group("/api")
  {
    folder := api.Group("folder")
    {
      folder.GET("/", GetRoot)
      folder.GET("/:id", GetFolder)
      folder.POST("/:parentid/:name", NewFolder)
      folder.DELETE("/:id", DeleteFolder)
      folder.PUT("/:id/:newname", RenameFolder)
    }
    note := api.Group("note")
    {
      note.GET("/:id", GetNote)
      note.POST("/:folder", NewNote)
      note.PUT("/:id", UpdateNote)
      note.DELETE("/:id", DeleteNote)
    }
  }

  fmt.Println("listening on http://" + C_Listen)
  r.Run(C_Listen)
}

