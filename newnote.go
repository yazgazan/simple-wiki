
package main

import (
  "github.com/gin-gonic/gin"
  "github.com/yazgazan/konek/arango-connector"
)

func NewNote(c *gin.Context) {
  doc := Note{}
  db := Arango()

  parentid := c.Params.ByName("folder")
  doc.Type = C_Type_Note

  if parentid == "" || parentid == "root" {
    root, err := RootDocument(db)
    if err != nil {
      if err, ok := err.(arango.StandardError); ok == true {
        c.JSON(err.Code, err)
      } else {
        c.JSON(500, err)
      }
      return
    }
    parentid = root.Key
  }
  note := db.NewDocument()
  err := note.Create("notes", doc)
  if err != nil {
    if err, ok := err.(arango.StandardError); ok == true {
      c.JSON(err.Code, err)
    } else {
      c.JSON(500, err)
    }
    return
  }

  edge := db.NewEdge()
  edge.From = "folders/" + parentid
  edge.To = note.Handle
  edge.Opt.CreateCollection = true
  err = edge.Create("hierarchy", Edge{Type:C_Type_Note})
  if err != nil {
    err2 := note.Delete()
    if err2 != nil {
      c.JSON(500, err2)
      return
    }
    if err, ok := err.(arango.StandardError); ok == true {
      c.JSON(err.Code, err)
    } else {
      c.JSON(500, err)
    }
    return
  }
  err = note.Fetch()
  if err != nil {
    if err, ok := err.(arango.StandardError); ok == true {
      c.JSON(err.Code, err)
    } else {
      c.JSON(500, err)
    }
    return
  }
  err = note.Read(&doc)
  if err != nil {
    c.JSON(500, err)
    return
  }
  c.JSON(201, doc)
}

