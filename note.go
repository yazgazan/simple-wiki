
package main

import (
  "github.com/yazgazan/konek/arango-connector"
)

type Note struct{
  Title string
  Data  string    `json:",omitempty"`
  Type  int
  Id    string    `json:"_key,omitempty"`

  Doc   *arango.Document  `json:"-"`
}

