
package main

import (
  "github.com/gin-gonic/gin"
  "github.com/yazgazan/konek/arango-connector"
)

func RootDocument(db *arango.Database) (*arango.Document, error) {
  collection := db.Collection("folders")
  example := Folder{}

  return collection.MatchExample(example)
}

func CreateTree(db *arango.Database, f *Folder) error {
  edges, err := f.Doc.EdgesOut("hierarchy")
  if err != nil {
    return err
  }
  count := 0
  for _, e := range edges {
    var edge Edge
    err = e.Read(&edge)
    if err != nil {
      return err
    }
    if edge.Type == C_Type_Folder {
      count++
    }
  }
  f.Kids = make([]Folder, count)
  i := 0
  for _, e := range edges {
    var edge Edge
    err = e.Read(&edge)
    if err != nil {
      return err
    }
    if edge.Type != C_Type_Folder {
      continue
    }
    doc, err := db.GetDocument(e.To)
    if err != nil {
      return err
    }
    err = doc.Read(&f.Kids[i])
    if err != nil {
      return err
    }
    f.Kids[i].Doc = doc
    err = CreateTree(db, &f.Kids[i])
    i++
  }
  return nil
}

func GetRoot(c *gin.Context) {
  var folders Folder
  db := Arango()
  root, err := RootDocument(db)

  if err != nil {
    if err, ok := err.(arango.StandardError); ok == true {
      c.JSON(err.Code, err)
    } else {
      c.JSON(500, err)
    }
    return
  }
  err = root.Read(&folders)
  if err != nil {
    c.JSON(500, err)
    return
  }
  folders.Doc = root
  err = CreateTree(db, &folders)
  if err != nil {
    if err, ok := err.(arango.StandardError); ok == true {
      c.JSON(err.Code, err)
    } else {
      c.JSON(500, err)
    }
    return
  }
  c.JSON(200, folders)
}

