
package main

import (
  "github.com/yazgazan/konek/arango-connector"
  wiki "github.com/yazgazan/simple-wiki"
)

const (
  C_Listen = "localhost:8085"

  C_Arango_Addr = "http://localhost:8529"
  C_Arango_User = "root"
  C_Arango_Passwd = "kidoo blur carcrash airplane"
  C_Arango_Db = "simple-wiki"
)

func Arango() *arango.Database {
  ar := arango.New(C_Arango_Addr)
  ar.User = C_Arango_User
  ar.Passwd = C_Arango_Passwd

  return ar.Use(C_Arango_Db)
}

func main() {
  db := Arango()

  err := db.Drop()
  if err != nil {
    if err, ok := err.(arango.StandardError); ok == true {
      if err.Code != arango.E_NotFound {
        panic(err)
      }
    } else {
      panic(err)
    }
  }

  err = db.CreateAuto()
  if err != nil {
    panic(err)
  }

  c := db.Collection("folders")
  err = c.CreateAuto()
  if err != nil {
    panic(err)
  }

  c = db.Collection("notes")
  err = c.CreateAuto()
  if err != nil {
    panic(err)
  }

  c = db.Collection("hierarchy")
  err = c.CreateAuto()
  if err != nil {
    panic(err)
  }

  folder := wiki.Folder{
    Name: "",
    Type: wiki.C_Type_Folder,
  }
  doc := db.NewDocument()
  err = doc.Create("folders", folder)
  if err != nil {
    panic(err)
  }
}

