
package main

import (
  "github.com/gin-gonic/gin"
  "github.com/yazgazan/konek/arango-connector"
)

func RenameFolder(c *gin.Context) {
  var patch Folder

  db := Arango()
  id := c.Params.ByName("id")
  patch.Name = c.Params.ByName("newname")
  patch.Type = C_Type_Folder

  doc, err := db.GetDocument("folders/" + id)
  if err != nil {
    if err, ok := err.(arango.StandardError); ok == true {
      c.JSON(err.Code, err)
    } else {
      c.JSON(500, err)
    }
    return
  }

  err = doc.Patch(patch)
  if err != nil {
    if err, ok := err.(arango.StandardError); ok == true {
      c.JSON(err.Code, err)
    } else {
      c.JSON(500, err)
    }
    return
  }

  c.JSON(202, Error{
    Code: 202,
    Message: "folder renamed successfuly",
  })
}

