
package main

import (
  "bytes"
  "io/ioutil"
  "encoding/base64"

  "github.com/gin-gonic/gin"
  "github.com/yazgazan/konek/arango-connector"
)

func UpdateNote(c *gin.Context) {
  var note Note
  var data []byte

  db := Arango()
  id := c.Params.ByName("id")
  doc, err := db.GetDocument("notes/" + id)
  if err != nil {
    if err, ok := err.(arango.StandardError); ok == true {
      c.JSON(err.Code, err)
    } else {
      c.JSON(500, err)
    }
    return
  }

  data, err = ioutil.ReadAll(c.Request.Body)
  if err != nil {
    c.JSON(500, err)
    return
  }
  note.Data = base64.StdEncoding.EncodeToString(data)

  lines := bytes.Split(data, []byte("\n"))
  for _, line := range lines {
    if len(line) == 0 {
      continue
    }
    note.Title = base64.StdEncoding.EncodeToString(line)
    break
  }
  note.Type = C_Type_Note
  err = doc.Patch(note)
  if err != nil {
    if err, ok := err.(arango.StandardError); ok == true {
      c.JSON(err.Code, err)
    } else {
      c.JSON(500, err)
    }
    return
  }
  c.JSON(200, Error{
    Code: 200,
    Message: "note updated",
  })
}

