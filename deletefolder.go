
package main

import (
  "github.com/gin-gonic/gin"
  "github.com/yazgazan/konek/arango-connector"
)

func DeleteFolderRec(db *arango.Database, doc *arango.Document) error {
  edges, err := doc.EdgesIn("hierarchy")
  if err != nil {
    return err
  }
  for _, edge := range edges {
    err = edge.Delete()
    if err != nil {
      return err
    }
  }

  edges, err = doc.EdgesOut("hierarchy")
  if err != nil {
    return err
  }
  for _, edge := range edges {
    kid, err := db.GetDocument(edge.To)
    if err != nil {
      return err
    }
    err = DeleteFolderRec(db, kid)
    if err != nil {
      return err
    }
  }
  return doc.Delete()
}

func DeleteFolder(c *gin.Context) {
  db := Arango()
  id := c.Params.ByName("id")

  doc, err := db.GetDocument("folders/" + id)
  if err != nil {
    if err, ok := err.(arango.StandardError); ok == true {
      c.JSON(err.Code, err)
    } else {
      c.JSON(500, err)
    }
    return
  }

  err = DeleteFolderRec(db, doc)
  if err != nil {
    if err, ok := err.(arango.StandardError); ok == true {
      c.JSON(err.Code, err)
    } else {
      c.JSON(500, err)
    }
    return
  }
  c.JSON(202, Error{
    Code: 202,
    Message: "folder(s) deleted successfuly",
  })
}

