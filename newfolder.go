
package main

import (
  "github.com/gin-gonic/gin"
  "github.com/yazgazan/konek/arango-connector"
)

func FolderExists(db *arango.Database, name string) bool {
  collection := db.Collection("folders")
  doc := Folder{
    Name: name,
  }
  _, err := collection.MatchExample(doc)
  if err != nil {
    return false
  }
  return true
}

func NewFolder(c *gin.Context) {
  doc := Folder{}
  db := Arango()

  parentid := c.Params.ByName("parentid")
  doc.Name = c.Params.ByName("name")
  doc.Type = C_Type_Folder

  if FolderExists(db, doc.Name) {
    c.JSON(409, Error{
      Code: 409,
      Message: "folder already exists",
    })
    return
  }
  if parentid == "" || parentid == "root" {
    root, err := RootDocument(db)
    if err != nil {
      if err, ok := err.(arango.StandardError); ok == true {
        c.JSON(err.Code, err)
      } else {
        c.JSON(500, err)
      }
      return
    }
    parentid = root.Key
  }
  folder := db.NewDocument()
  err := folder.Create("folders", doc)
  if err != nil {
    if err, ok := err.(arango.StandardError); ok == true {
      c.JSON(err.Code, err)
    } else {
      c.JSON(500, err)
    }
    return
  }

  edge := db.NewEdge()
  edge.From = "folders/" + parentid
  edge.To = folder.Handle
  edge.Opt.CreateCollection = true
  err = edge.Create("hierarchy", Edge{Type:C_Type_Folder})
  if err != nil {
    err2 := folder.Delete()
    if err2 != nil {
      c.JSON(500, err2)
      return
    }
    if err, ok := err.(arango.StandardError); ok == true {
      c.JSON(err.Code, err)
    } else {
      c.JSON(500, err)
    }
    return
  }
  err = folder.Fetch()
  if err != nil {
    if err, ok := err.(arango.StandardError); ok == true {
      c.JSON(err.Code, err)
    } else {
      c.JSON(500, err)
    }
    return
  }
  err = folder.Read(&doc)
  if err != nil {
    c.JSON(500, err)
    return
  }
  c.JSON(201, doc)
}

