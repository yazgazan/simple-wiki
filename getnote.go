
package main

import (
  "encoding/base64"

  "github.com/gin-gonic/gin"
  "github.com/yazgazan/konek/arango-connector"
)

func GetNote(c *gin.Context) {
  note := Note{}
  db := Arango()
  id := c.Params.ByName("id")

  doc, err := db.GetDocument("notes/"+id)
  if err != nil {
    if err, ok := err.(arango.StandardError); ok == true {
      c.JSON(err.Code, err)
    } else {
      c.JSON(500, err)
    }
    return
  }

  err = doc.Read(&note)
  if err != nil {
    if err, ok := err.(arango.StandardError); ok == true {
      c.JSON(err.Code, err)
    } else {
      c.JSON(500, err)
    }
    return
  }
  data, err := base64.StdEncoding.DecodeString(note.Data)
  if err != nil {
    if err, ok := err.(arango.StandardError); ok == true {
      c.JSON(err.Code, err)
    } else {
      c.JSON(500, err)
    }
    return
  }
  c.Data(200, "text/plain", data)
}

