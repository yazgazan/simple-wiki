
package main

import (
  "github.com/yazgazan/konek/arango-connector"
)

const (
  C_Type_Folder = 0
  C_Type_Note   = 1
)

type Folder struct{
  Name  string
  Type  int
  Id    string    `json:"_key,omitempty"`
  Kids  []Folder  `json:",omitempty"`

  Doc   *arango.Document  `json:"-"`
}

