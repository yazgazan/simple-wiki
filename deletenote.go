
package main

import (
  "github.com/gin-gonic/gin"
  "github.com/yazgazan/konek/arango-connector"
)

func DeleteNote(c *gin.Context) {
  db := Arango()
  id := c.Params.ByName("id")

  doc, err := db.GetDocument("notes/" + id)
  if err != nil {
    if err, ok := err.(arango.StandardError); ok == true {
      c.JSON(err.Code, err)
    } else {
      c.JSON(500, err)
    }
    return
  }

  edges, err := doc.EdgesIn("hierarchy")
  if err != nil {
    if err, ok := err.(arango.StandardError); ok == true {
      c.JSON(err.Code, err)
    } else {
      c.JSON(500, err)
    }
    return
  }
  for _, edge := range edges {
    err = edge.Delete()
    if err != nil {
      if err, ok := err.(arango.StandardError); ok == true {
        c.JSON(err.Code, err)
      } else {
        c.JSON(500, err)
      }
      return
    }
  }

  err = doc.Delete()
  if err != nil {
    if err, ok := err.(arango.StandardError); ok == true {
      c.JSON(err.Code, err)
    } else {
      c.JSON(500, err)
    }
    return
  }
  c.JSON(200, Error{
    Code: 200,
    Message: "note deleted",
  })
}

