
package main

import (
  "fmt"

  "github.com/gin-gonic/gin"
  "github.com/yazgazan/konek/arango-connector"
)

func GetFolder(c *gin.Context) {
  var notes []Note

  db := Arango()
  id := c.Params.ByName("id")

  doc, err := db.GetDocument("folders/" + id)
  if err != nil {
    if err, ok := err.(arango.StandardError); ok == true {
      c.JSON(err.Code, err)
    } else {
      c.JSON(500, err)
    }
    return
  }

  edges, err := doc.EdgesOut("hierarchy")
  fmt.Println(len(edges))
  if err != nil {
    if err, ok := err.(arango.StandardError); ok == true {
      c.JSON(err.Code, err)
    } else {
      c.JSON(500, err)
    }
    return
  }
  count := 0
  for _, e := range edges {
    var edge Edge
    err := e.Read(&edge)
    if err != nil {
      if err, ok := err.(arango.StandardError); ok == true {
        c.JSON(err.Code, err)
      } else {
        c.JSON(500, err)
      }
      return
    }
    if edge.Type == C_Type_Note {
      count++
    }
  }

  fmt.Println(count)
  notes = make([]Note, count)
  i := 0
  for _, e := range edges {
    var edge Edge
    err = e.Read(&edge)
    if err != nil {
      if err, ok := err.(arango.StandardError); ok == true {
        c.JSON(err.Code, err)
      } else {
        c.JSON(500, err)
      }
      return
    }
    if edge.Type == C_Type_Folder {
      continue
    }
    note, err := db.GetDocument(edge.To)
    if err != nil {
      if err, ok := err.(arango.StandardError); ok == true {
        c.JSON(err.Code, err)
      } else {
        c.JSON(500, err)
      }
      return
    }
    note.Read(&notes[i])
    notes[i].Doc = note
    notes[i].Data = ""
    i++
  }
  c.JSON(200, notes)
}

